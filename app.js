const express = require('express');
const http = require('http'); 
const path = require('path');
const logger = require('morgan');
const cors = require('cors');
const { Server } = require('socket.io');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const recipesRouter = require('./routes/api/recipes');
const areasRouter = require('./routes/api/areas');
const categoriesRouter = require('./routes/api/categories');
const eventsRouter = require('./routes/api/events');
const ingredientsRouter = require('./routes/api/ingredients');
const ordersRouter = require('./routes/api/orders');

const app = express();
const formatsLogger = app.get('env') === 'development' ? 'dev' : 'short';

const server = http.createServer(app);
const io = new Server(server, {
  cors: {
    origin: '*',
  },
});

io.on('connection', (socket) => {
  console.log('New frontend connection');

  socket.on('testSockets', async () => {
    console.log('Test sockets');
  });
});

app.use(logger(formatsLogger));
app.use(cors());
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use('/api/recipes', recipesRouter);
app.use('/api/areas', areasRouter);
app.use('/api/categories', categoriesRouter);
app.use('/api/events', eventsRouter);
app.use('/api/ingredients', ingredientsRouter);
app.use('/api/orders', ordersRouter);

app.use((req, res) => {
  res.status(404).json({ message: 'Service not found' });
});


app.use((err, req, res, next) => {
  const { status = 500, message = 'Server error' } = err;
  res.status(status).json({ message });
});

module.exports = server;  
