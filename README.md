# Tasty Treats Backend

Бекенд для запитів до пет-проєкту 'Tasty Treats'
[Документація](https://tasty-treats-backend.onrender.com/api-docs/)

Всі материали до проєкту:
[Макет](https://www.figma.com/file/DfLTK1bjDwKKxkEJ1FTyFF/TastyTreats?type=design&node-id=0-1&t=MJN7ruWcjYM08LVE-0)
[Технічне завдання](https://docs.google.com/spreadsheets/d/18AiL3Qi3EPQ-0-WW5roQ5va287xnO_d1721FsaQZO6c/edit#gid=0)

