const { Schema, model } = require('mongoose');

const ingredientSchema = new Schema({ _id: String, name: String, desc: String, img: String });

const Ingredient = model('ingredient', ingredientSchema);

module.exports = Ingredient;