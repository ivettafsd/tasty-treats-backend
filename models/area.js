const { Schema, model } = require('mongoose');
const areaSchema = new Schema({ name: String });

const Area = model("area", areaSchema);

module.exports = Area;
