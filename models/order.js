const { handleMangooseError } = require('../helpers');
const { Schema, model } = require('mongoose');
const Joi = require('joi');

const phoneRegexp = /(?=.*\+[0-9]{3}\s?[0-9]{2}\s?[0-9]{3}\s?[0-9]{4,5}$)/;
const emailRegexp = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

const orderSchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'Set name'],
    },
    phone: {
      type: String,
      match: phoneRegexp,
      required: [true, 'Set phone'],
    },
    email: {
      type: String,
      match: emailRegexp,
      required: [true, 'Set email'],
    },
    comment: {
      type: String,
      required: false,
    },
  },
  { versionKey: false, timestamps: true },
);

orderSchema.post('save', handleMangooseError);

const addSchema = Joi.object({
  name: Joi.string().required().messages({
    'string.base': 'The name must be a string.',
    'any.required': 'The name field is required.',
  }),
  phone: Joi.string().pattern(phoneRegexp).required().messages({
    'string.base': 'The phone must be a string.',
    'string.pattern.base': 'The phone must be in format +380000000000.',
    'any.required': 'The phone field is required.',
  }),
  email: Joi.string().pattern(emailRegexp).required().messages({
    'string.base': 'The email must be a string.',
    'string.pattern.base': 'The email must be in format test@gmail.com.',
    'any.required': 'The email field is required.',
  }),
  comment: Joi.string().optional().messages({
    'string.base': 'The comment must be a string.',
  }),
});

const Order = model('order', orderSchema);
const schemas = { addSchema };

module.exports = { Order, schemas };
