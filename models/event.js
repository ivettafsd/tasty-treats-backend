const { Schema, model } = require('mongoose');

const eventSchema = new Schema({
  _id: String,
  cook: {
    name: {
      type: String,
      required: true,
    },
    imgUrl: {
      type: String,
      required: true,
    },
  },
  topic: {
    name: {
      type: String,
      required: true,
    },
    area: {
      type: String,
      required: true,
    },
    previewUrl: {
      type: String,
      required: true,
    },
    imgUrl: {
      type: String,
      required: true,
    },
  },
});

const Event = model('event', eventSchema);

module.exports = Event;