const { handleMangooseError } = require('../helpers');
const { Schema, model } = require('mongoose');
const Joi = require('joi');

const emailRegexp = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

const recipeSchema = new Schema(
  {
    title: {
      type: String,
      unique: true,
      required: true,
    },
    category: {
      type: String,
      required: true,
    },
    area: {
      type: String,
      required: true,
    },
    instructions: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    thumb: {
      type: String,
      required: true,
    },
    preview: {
      type: String,
      required: true,
    },
    time: {
      type: String,
      required: true,
    },
    youtube: {
      type: String,
      required: false,
    },
    tags: {
      type: [String],
      required: false,
    },
    ingredients: {
      _id: false,
      type: [
        {
          id: {
            type: Schema.Types.ObjectId,
            ref: 'ingredient',
          },
          measure: String,
        },
      ],
      required: true,
    },
    rating: {
      type: Number,
      required: true,
    },
    whoRated: {
      type: [String],
      required: true,
    },
  },
  { versionKey: false, timestamps: true },
);

recipeSchema.post('save', handleMangooseError);

const addSchema = Joi.object({
  title: Joi.string().required().messages({
    'string.base': 'The title must be a string.',
    'any.required': 'The title field is required.',
  }),
  category: Joi.string().required().messages({
    'string.base': 'The category must be a string.',
    'any.required': 'The category field is required.',
  }),
  area: Joi.string().required().messages({
    'string.base': 'The area must be a string.',
    'any.required': 'The area field is required.',
  }),
  instructions: Joi.string().required().messages({
    'string.base': 'The instructions must be a string.',
    'any.required': 'The instructions field is required.',
  }),
  description: Joi.string().required().messages({
    'string.base': 'The description must be a string.',
    'any.required': 'The description field is required.',
  }),
  thumb: Joi.string().required().messages({
    'string.base': 'The thumb must be a string.',
    'any.required': 'The thumb field is required.',
  }),
  preview: Joi.string().required().messages({
    'string.base': 'The preview must be a string.',
    'any.required': 'The preview field is required.',
  }),
  time: Joi.string().required().messages({
    'string.base': 'The time must be a string.',
    'any.required': 'The time field is required.',
  }),
  youtube: Joi.alternatives().try(Joi.string(), Joi.allow(null)).messages({
    'alternatives.types': 'The youtube field must be a string or null.',
  }),
  tags: Joi.array().items(Joi.string()).messages({
    'array.base': 'The tags must be an array.',
    'array.items': 'Each tag must be a string.',
  }),
  ingredients: Joi.array()
    .items(
      Joi.object({
        id: Joi.string(),
        measure: Joi.string(),
      }),
    )
    .required()
    .messages({
      'array.base': 'The ingredients must be an array.',
      'array.items': 'Each ingredient must be an object with "id" and "measure" properties.',
      'any.required': 'The ingredients field is required.',
    }),
});

const addRateSchema = Joi.object({
  rate: Joi.number().strict().required().min(1).max(5).messages({
    'number.base': 'The rate must be a number.',
    'number.min': 'The rate must be at least 1.',
    'number.max': 'The rate cannot exceed 5.',
    'any.required': 'The rate field is required.',
  }),
  email: Joi.string().pattern(emailRegexp).required().messages({
    'string.base': 'The email must be a string.',
    'string.pattern.base': 'The email must be in format test@gmail.com.',
    'any.required': 'The email field is required.',
  }),
});

const Recipe = model('recipe', recipeSchema);
const schemas = { addSchema, addRateSchema };

module.exports = { Recipe, schemas };