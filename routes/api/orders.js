const express = require('express');
const ctrl = require('../../controllers/orders');
const { validateBody } = require('../../middlewares');
const { schemas } = require('../../models/order');

const router = express.Router();

router.post('/add', validateBody(schemas.addSchema), ctrl.addOrder);

module.exports = router;
