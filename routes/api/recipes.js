const express = require('express');
const ctrl = require('../../controllers/recipes');
const { validateBody, isValidId } = require('../../middlewares');
const { schemas } = require('../../models/recipe');

const router = express.Router();

router.get('/', ctrl.getAllRecipes);
router.post('/add', validateBody(schemas.addSchema), ctrl.addRecipe);
router.get('/popular', ctrl.getPopularRecipes);
router.patch('/:id/rating', isValidId, validateBody(schemas.addRateSchema), ctrl.addRatingById);
router.get('/:id',isValidId, ctrl.getRecipeById);

module.exports = router;
