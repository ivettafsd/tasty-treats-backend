const express = require('express');
const ctrl = require('../../controllers/areas')

const router = express.Router();

router.get('/', ctrl.getAreas);

module.exports = router;
