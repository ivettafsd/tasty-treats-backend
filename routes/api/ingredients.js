const express = require('express');
const ctrl = require('../../controllers/ingredients')

const router = express.Router();

router.get('/', ctrl.getIngredients);

module.exports = router;
