const express = require('express');
const ctrl = require('../../controllers/events')

const router = express.Router();

router.get('/', ctrl.getEvents);

module.exports = router;