const Area = require('../models/area');

const getAreas = async (req, res, next) => {
  try {
    const allAreas = await Area.find();
    res.json(allAreas);
  } catch (err) {
    next(err);
  }
};

module.exports = { getAreas };