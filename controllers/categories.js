const Category = require('../models/category');

const getCategories = async (req, res, next) => {
  try {
    const allCategories = await Category.find().sort({ name: 1 });
    res.json(allCategories);
  } catch (err) {
    next(err);
  }
};

module.exports = { getCategories };