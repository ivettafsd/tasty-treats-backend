const { HttpError, CtrlWrapper } = require('../helpers');
const { Recipe } = require('../models/recipe');
const { Types } = require('mongoose');
const ObjectId = Types.ObjectId;
  
const getAllRecipes = async (req, res) => {
  const { category, area, time, ingredient, title, page = 1, limit = 6 } = req.query;

  const query = {};
  category && (query.category = category);
  area && (query.area = area);
  time && (query.time = time);
  ingredient && (query.ingredients = { $elemMatch: { id: ingredient } });
  title && (query.title = { $regex: title, $options: 'i' });

  const skip = (page - 1) * limit;
  const foundRecipes = await Recipe.aggregate([
    { $match: query },
    {
      $facet: {
        totalCount: [{ $count: 'count' }],
        results: [{ $skip: Number(skip) }, { $limit: Number(limit) }, { $unset: ["whoRated", "createdAt", "updatedAt"] }],
      },
    },
    {
      $project: {
        totalCount: { $arrayElemAt: ['$totalCount.count', 0] },
        page: page,
        perPage: limit,
        results: 1,
      },
    },
  ]);

  res.json({
    page,
    perPage: limit,
    totalPages: Math.ceil(foundRecipes[0].totalCount / limit),
    results: foundRecipes[0].results,
  });
};

const addRecipe = async (req, res) => {
  const newRecipe = req.body;
  const updatedRecipe = {
    ...newRecipe,
    rating: 5,
    whoRated: ['author@gmail.com'],
  };
  const recipe = await Recipe.create(updatedRecipe);
  res.status(201).json(recipe);
};

const getPopularRecipes = async (req, res) => {
  const popularRecipes = await Recipe.aggregate([
    {
      $project: {
        _id: 1,
        title: 1,
        description: 1,
        preview: 1,
        thumb: 1,
        popularity: { $multiply: ['$rating', { $size: '$whoRated' }] },
      },
    },
    { $sort: { popularity: -1 } },
    { $limit: 4 },
  ]);
  res.json(popularRecipes);
};

const getRecipeById = async (req, res) => {
  const { id } = req.params;

  const recipe = await Recipe.aggregate([
    {
      $match: {
        _id: ObjectId.createFromHexString(id),
      },
    },
    {
      $lookup: {
        from: 'ingredients',
        localField: 'ingredients.id',
        foreignField: '_id',
        as: 'ingr_nfo',
      },
    },
    {
      $set: {
        ingredients: {
          $map: {
            input: '$ingredients',
            in: {
              $mergeObjects: [
                '$$this',
                {
                  $arrayElemAt: [
                    '$ingr_nfo',
                    {
                      $indexOfArray: ['$ingr_nfo._id', '$$this.id'],
                    },
                  ],
                },
              ],
            },
          },
        },
        whoRated: {
          $size: '$whoRated',
        },
      },
    },
    {
      $unset: ['ingr_nfo', 'ingredients._id', 'createdAt', 'updatedAt'],
    },
  ]);

  if (recipe.length === 0) {
    throw HttpError(404, 'Not found');
  }
  res.json(recipe[0]);
};

const addRatingById = async (req, res) => {
  const { id } = req.params;
  const newRate = req.body;
  const foundRecipe = await Recipe.findById(id);
  const isOldUser = foundRecipe.whoRated.includes(newRate.email);
  if (isOldUser) {
    throw HttpError(409, 'Such email already exists');
  }
  const recipeWithNewRating = {
    rating: +((foundRecipe.rating * foundRecipe.whoRated.length + newRate.rate) / (foundRecipe.whoRated.length + 1)).toFixed(2),
    whoRated: [...foundRecipe.whoRated, newRate.email],
  };
  await Recipe.findByIdAndUpdate(id, recipeWithNewRating, { new: true });
  const recipe = await Recipe.aggregate([
    {
      $match: {
        _id: ObjectId.createFromHexString(id),
      },
    },
    {
      $lookup: {
        from: 'ingredients',
        localField: 'ingredients.id',
        foreignField: '_id',
        as: 'ingr_nfo',
      },
    },
    {
      $set: {
        ingredients: {
          $map: {
            input: '$ingredients',
            in: {
              $mergeObjects: [
                '$$this',
                {
                  $arrayElemAt: [
                    '$ingr_nfo',
                    {
                      $indexOfArray: ['$ingr_nfo._id', '$$this.id'],
                    },
                  ],
                },
              ],
            },
          },
        },
        whoRated: {
          $size: '$whoRated',
        },
      },
    },
    {
      $unset: ['ingr_nfo', 'ingredients._id', 'createdAt', 'updatedAt'],
    },
  ]);
  res.json(recipe[0]);
};

module.exports = {
  getAllRecipes: CtrlWrapper(getAllRecipes),
  addRecipe: CtrlWrapper(addRecipe),
  getPopularRecipes: CtrlWrapper(getPopularRecipes),
  addRatingById: CtrlWrapper(addRatingById),
  getRecipeById: CtrlWrapper(getRecipeById),
};
