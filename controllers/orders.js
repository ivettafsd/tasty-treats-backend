const { CtrlWrapper } = require('../helpers');
const { Order } = require('../models/order');

const addOrder = async (req, res) => {
  const newOrder = req.body;
  await Order.create(newOrder);
  res.status(201).json({message: 'Success'});
};

module.exports = {
  addOrder: CtrlWrapper(addOrder),
};