const Ingredient = require('../models/ingredient');

const getIngredients = async (req, res, next) => {
  try {
    const allIngredients = await Ingredient.find();
    res.json(allIngredients);
  } catch (err) {
    next(err);
  }
};

module.exports = { getIngredients };