const Event = require('../models/event');

const getEvents = async (req, res, next) => {
  try {
    const allEvents = await Event.find();
    res.json(allEvents);
  } catch (err) {
    next(err);
  }
};

module.exports = { getEvents };